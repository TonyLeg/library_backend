FROM python:3.8-bookworm
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1
WORKDIR /app
COPY requirements.txt /app
RUN pip install --upgrade pip
RUN pip install -r /app/requirements.txt
COPY . /app
RUN chmod +x /app/start.sh
EXPOSE 8000
CMD ["/bin/bash", "-c", "python3 parse_docx.py;python3 manage.py makemigrations;python3 manage.py migrate;python3 manage.py runserver 0.0.0.0:8000"]